int pinLedR = 8;  // pin Rojo del led RGB
int pinLedV = 9;  // pin Verde del led RGB
int pinLedA = 10;   // pin Azul del led RGB

int pausa = 500;

void setup() {
  pinMode(pinLedR, OUTPUT);    // pone el pinLedR como output
  pinMode(pinLedV, OUTPUT);    // pone el pinLedV como output
  pinMode(pinLedA, OUTPUT);    // pone el pinLedA como output
}

void loop() {
  for(int i=0;i<=255;i+=10){
    for(int j=0;j<=255;j+=10){
      for(int k=0;k<=255;k+=10){
        color(i,j,k);
        delay(pausa);
      }  
    }    
  }
}

// funcion para generar colores
void color (int rojo, int verde, int azul) {
  analogWrite(pinLedR, rojo);
  analogWrite(pinLedV, verde);
  analogWrite(pinLedA, azul);
}
